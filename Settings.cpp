#include "Settings.h"

#include <QWidget>
#include <QDebug>
#include <QDir>
#include <QFile>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QMessageBox>

Settings::Settings( QWidget* parent )
: QWidget( parent )
{
    ui.setupUi( this );

    ui.style->addItem("file");
    ui.style->addItem("LLVM");
    ui.style->addItem("Google");
    ui.style->addItem("Chromium");
    ui.style->addItem("Mozilla");
    ui.style->addItem("WebKit");

    connect(ui.config_btn, SIGNAL(clicked()), this, SLOT(save_config_slot()));
    connect(ui.clear_btn, SIGNAL(clicked()), this, SLOT(clear_config_slot()));
    connect(ui.load_btn, SIGNAL(clicked()), this, SLOT(load_config_slot()));
}

Settings::~Settings() {}

bool Settings::load_config(const QString& pluginFilePath)
{
    QString pluginPath = pluginFilePath;
    m_config_path = pluginPath;
    QString confFilePath = QString("%1/%2").arg(pluginPath).arg("ndd-clang-format-plugin.json");

    QFile file(confFilePath);
    if (!file.exists())
    {
        qDebug() << "conf.json not found.";
        return false;
    }

    if (!file.open(QIODevice::ReadOnly))
    {
        qDebug() << "open conf.json failed.";
        return false;
    }

    QJsonParseError error;
    auto jsonDoc = QJsonDocument::fromJson(file.readAll(), &error);
    if (error.error != QJsonParseError::NoError)
    {
        qDebug() << "parse conf.json failed.";
        return false;
    }

    auto root = jsonDoc.object();

    if (root.contains("clang-format"))
    {
        auto config_detail = root["clang-format"].toObject();

    
        if (config_detail["style"].isUndefined() || !config_detail["style"].isString())
            return false;

        if (config_detail["assumeFilename"].isUndefined()
            || !config_detail["assumeFilename"].isString())
            return false;

        if (config_detail["executable"].isUndefined() || !config_detail["executable"].isString())
            return false;

        set_style(config_detail["style"].toString());
        set_assume_filename(config_detail["assumeFilename"].toString());
        set_executable(config_detail["executable"].toString());

        file.flush();
    }

    return true;
}

bool Settings::save_config_slot()
{
    QString confFilePath = QString("%1/%2").arg(m_config_path).arg("ndd-clang-format-plugin.json");
    QFile file(confFilePath);
    if (!file.exists())
    {
        QMessageBox::critical(this, "Error", "conf.json not found.");
        qDebug() << "conf.json not found.";
        return false;
    }

    if (!file.open(QIODevice::WriteOnly))
    {
        QMessageBox::critical(this, "Error", "open conf.json failed.");
        qDebug() << "open conf.json failed.";
        return false;
    }

    QJsonObject config_detail;
    config_detail["style"] = get_style();
    config_detail["assumeFilename"] = get_assume_filename();
    config_detail["executable"] = get_executable();
    QJsonObject config;
    config["clang-format"] = config_detail;

    QJsonDocument jdoc(config);
    file.write(jdoc.toJson());

    file.flush();

    QMessageBox::information(this, "Tips", "Save config successfully!");

    return true;
}

bool Settings::clear_config_slot()
{
    ui.style->setCurrentIndex(0);
    ui.exec_path->setText("");
    ui.filename->setText("");

    QMessageBox::information(this, "Tips", "Clear config successfully!");

    return true;
}

bool Settings::load_config_slot()
{
    load_config(m_config_path);
    
    QMessageBox::information(this, "Tips", "Load config successfully!");

    return true;
}
