# NotePad-- clang-format 格式化插件

> NotePad-- 上 clang-format 代码格式化插件

## 安装

把 `clang-format.dll ` 和配置文件 `ndd-clang-format-plugin.json(配置文件)` 放入编辑器的插件目录即可。

## 插件设置

`style`：一些预定格式，包括 file, LLVM, Google, Chromium, Mozilla, WebKit .

`assumeFilename`：在 `style` 设置为 `file` 时，使用的 `.clang-format` 文件路径。

`excutable`：clang-format 可执行文件路径，为空默认为系统环境变量。

![1697905148723](image/readme/1697905148723.png)

## 使用方法：

点击菜单栏：

![1697905053389](image/readme/1697905053389.png)

`Format file` 格式化整个文件。

`Format selection` 格式化选中的内容。

`Settings` 打开设置窗口。

**在格式化之前先进行保存操作。**

## 配置文件

```json
{
  "clang-format": {
    "assumeFilename": "C:\\Users\\songz\\Desktop\\format_test\\.clang-format",
    "executable": "23",
    "style": "file"
  }
}
```

具体配置项的含义与前面插件的设置相同。
