/********************************************************************************
** Form generated from reading UI file 'setting_win.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SETTING_WIN_H
#define UI_SETTING_WIN_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_settings_win
{
public:
    QGridLayout *gridLayout;
    QVBoxLayout *verticalLayout;
    QLabel *label;
    QSpacerItem *verticalSpacer;
    QHBoxLayout *horizontalLayout;
    QLabel *label_3;
    QSpacerItem *horizontalSpacer_2;
    QComboBox *style;
    QSpacerItem *horizontalSpacer;
    QSpacerItem *verticalSpacer_3;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label_4;
    QLineEdit *filename;
    QSpacerItem *verticalSpacer_4;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label_5;
    QLineEdit *exec_path;
    QSpacerItem *verticalSpacer_5;
    QHBoxLayout *horizontalLayout_5;
    QPushButton *config_btn;
    QPushButton *clear_btn;
    QPushButton *load_btn;

    void setupUi(QWidget *settings_win)
    {
        if (settings_win->objectName().isEmpty())
            settings_win->setObjectName(QString::fromUtf8("settings_win"));
        settings_win->resize(303, 356);
        gridLayout = new QGridLayout(settings_win);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(6);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        label = new QLabel(settings_win);
        label->setObjectName(QString::fromUtf8("label"));
        QFont font;
        font.setPointSize(12);
        font.setBold(true);
        label->setFont(font);

        verticalLayout->addWidget(label);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label_3 = new QLabel(settings_win);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        horizontalLayout->addWidget(label_3);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);

        style = new QComboBox(settings_win);
        style->setObjectName(QString::fromUtf8("style"));

        horizontalLayout->addWidget(style);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);


        verticalLayout->addLayout(horizontalLayout);

        verticalSpacer_3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer_3);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        label_4 = new QLabel(settings_win);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        horizontalLayout_3->addWidget(label_4);

        filename = new QLineEdit(settings_win);
        filename->setObjectName(QString::fromUtf8("filename"));

        horizontalLayout_3->addWidget(filename);


        verticalLayout->addLayout(horizontalLayout_3);

        verticalSpacer_4 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer_4);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        label_5 = new QLabel(settings_win);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        horizontalLayout_4->addWidget(label_5);

        exec_path = new QLineEdit(settings_win);
        exec_path->setObjectName(QString::fromUtf8("exec_path"));

        horizontalLayout_4->addWidget(exec_path);


        verticalLayout->addLayout(horizontalLayout_4);

        verticalSpacer_5 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer_5);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setSpacing(6);
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        config_btn = new QPushButton(settings_win);
        config_btn->setObjectName(QString::fromUtf8("config_btn"));

        horizontalLayout_5->addWidget(config_btn);

        clear_btn = new QPushButton(settings_win);
        clear_btn->setObjectName(QString::fromUtf8("clear_btn"));

        horizontalLayout_5->addWidget(clear_btn);

        load_btn = new QPushButton(settings_win);
        load_btn->setObjectName(QString::fromUtf8("load_btn"));

        horizontalLayout_5->addWidget(load_btn);


        verticalLayout->addLayout(horizontalLayout_5);


        gridLayout->addLayout(verticalLayout, 0, 0, 1, 1);


        retranslateUi(settings_win);

        QMetaObject::connectSlotsByName(settings_win);
    } // setupUi

    void retranslateUi(QWidget *settings_win)
    {
        settings_win->setWindowTitle(QCoreApplication::translate("settings_win", "Settings", nullptr));
        label->setText(QCoreApplication::translate("settings_win", "clang-fomat plugin", nullptr));
        label_3->setText(QCoreApplication::translate("settings_win", "style:", nullptr));
        label_4->setText(QCoreApplication::translate("settings_win", "assumeFilename:", nullptr));
        label_5->setText(QCoreApplication::translate("settings_win", "executable:", nullptr));
        config_btn->setText(QCoreApplication::translate("settings_win", "config", nullptr));
        clear_btn->setText(QCoreApplication::translate("settings_win", "clear", nullptr));
        load_btn->setText(QCoreApplication::translate("settings_win", "load", nullptr));
    } // retranslateUi

};

namespace Ui {
    class settings_win: public Ui_settings_win {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SETTING_WIN_H
