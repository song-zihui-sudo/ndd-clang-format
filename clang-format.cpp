#include <QProcess>
#include <QDebug>
#include <QMessageBox>

#include "clang-format.h"
#include "Settings.h"

clang_format::clang_format(QWidget* s_pMainNotepad) : QWidget(s_pMainNotepad)
{ 
    m_config = new Settings(s_pMainNotepad); 
}

clang_format::clang_format( clang_format&& other ) : QWidget(other.m_config->parentWidget())
{
    if ( this != &other )
    {
        delete m_config;
        m_config       = other.m_config;
        other.m_config = NULL;
    }
}

clang_format::~clang_format() { delete m_config; }

clang_format& clang_format::operator=( clang_format&& other )
{
    if ( this != &other )
    {
        delete m_config;
        m_config       = other.m_config;
        other.m_config = NULL;
    }
    return *this;
}

bool clang_format::load_config(const QString strFileName)
{
    QString plugin_path = strFileName;
    plugin_path.remove(strFileName.lastIndexOf('\\'), strFileName.size() - strFileName.lastIndexOf('\\'));
    return m_config->load_config(plugin_path);
}

void clang_format::config()
{
    m_config->setWindowFlag( Qt::Window );
    m_config->show();
}

void clang_format::format(std::function<QsciScintilla* ()> s_getCurEdit)
{
    QString format_mode = mode();

    if (!s_getCurEdit)
    {
        return;
    }

    QsciScintilla* doc = s_getCurEdit();
    
    if (!doc)
    {
        return;
    }

    /* 获取文件路径 */
    QString window_file_path = get_file_path();
    if (window_file_path.isEmpty())
    {
        qDebug() << "save file first!"; 
        QMessageBox::critical(this, "Error", "clang-format: save file first!");
        return;
    }
    format_mode += "-i " + window_file_path;
    QProcess p(0);
    p.start(format_mode);
    p.waitForFinished(); /* 等待完成 */
    QString error = QString::fromLocal8Bit(p.readAllStandardError());
    qDebug() << error;
    if (!error.isEmpty())
    {
        QMessageBox::critical(this, "Error", error);
    }
}

void clang_format::format_selection(std::function<QsciScintilla* ()> s_getCurEdit) 
{
    QString format_mode = mode();

    if (!s_getCurEdit)
    {
        return;
    }

    QsciScintilla* doc = s_getCurEdit();

    if (!doc)
    {
        return;
    }

    format_mode += "-lines=";
    /* 获取选中行的起始行号 */
    int lineFrom, indexFrom = -1, lineTo, indexTo = -1;
    doc->getSelection(&lineFrom, &indexFrom, &lineTo, &indexTo);
    format_mode += QString::number(lineFrom + 1) + ":" + QString::number(lineTo + 1);

    /* 获取文件路径 */
    QString window_file_path = get_file_path();
    if (window_file_path.isEmpty())
    {
        qDebug() << "save file first!";
        QMessageBox::critical(this, "Error", "clang-format: save file first!");
        return;
    }

    format_mode += " -i " + window_file_path;

    QProcess p(0);
    p.start(format_mode);
    p.waitForFinished(); /* 等待完成 */
    QString error = QString::fromLocal8Bit(p.readAllStandardError());
    qDebug() << error;
    if (!error.isEmpty())
    {
        QMessageBox::critical(this, "Error", error);
    }
}

QString clang_format::mode()
{
    QString mode = "clang-format";
    
    if (!m_config->get_executable().isEmpty())
    {
        mode = m_config->get_executable() + "/" + mode;
    }

    if (m_config->get_style() != "file")
    {
        mode += " -style=" + m_config->get_style();
    }
    else
    {
        mode += " -style=file";
        mode += " -assume-filename=" + m_config->get_assume_filename();
    }

    mode += " ";

    return mode;
}

QString clang_format::get_file_path()
{
    QString path = parentWidget()->windowTitle();
    quint32 index = path.lastIndexOf('[');
    quint32 count = path.size() - index;
    path.remove(index, count);
    return path;
}