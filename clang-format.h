#ifndef clang_format_h
#define clang_format_h

#include <QObject>
#include <QString>
#include <qsciscintilla.h>

class Settings;

class clang_format : public QWidget
{
public:
    clang_format(QWidget* s_pMainNotepad);

    clang_format( clang_format&& source );

    ~clang_format();

    clang_format& operator=( clang_format&& source );

public:
    bool load_config(const QString strFileName);

    void config();

    void format(std::function<QsciScintilla* ()> s_getCurEdit);

    void format_selection(std::function<QsciScintilla* ()> s_getCurEdit);

    QString mode();
    
    QString get_file_path();

private:
    Settings* m_config;
};

extern clang_format* clang_format_plugin;

#endif
