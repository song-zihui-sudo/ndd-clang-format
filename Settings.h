#define Settings_h
#ifdef Settings_h

#include <QString>
#include <QWidget>

#include "ui_setting_win.h"

class Settings : public QWidget
{
	Q_OBJECT

public:
	Settings(QWidget* parent = nullptr);
	~Settings();

public:
	bool load_config(const QString& pluginFilePath);

	QString get_style() 
	{ 
		return ui.style->currentText();
	};

	QString get_assume_filename() 
	{ 
		return ui.filename->text();;
	}

	QString get_executable() 
	{ 
		return ui.exec_path->text();
	}

	void set_style(QString style) 
	{ 
		ui.style->setCurrentText(style);
	}

	void set_assume_filename(QString file_path) 
	{ 
		ui.filename->setText(file_path);
	}

	void set_executable(QString executable) 
	{ 
		ui.exec_path->setText(executable);
	}

private slots:
	bool save_config_slot();

	bool clear_config_slot();

	bool load_config_slot();

private:
	Ui::settings_win ui;
	QString m_config_path = "./";
};


#endif
